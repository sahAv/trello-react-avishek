import { BrowserRouter, Route, Routes } from "react-router-dom";
import ListContainer from "./components/List/ListContainer";
import BoardContent from "./components/Boards/BoardContent";
import Navbar from "./components/Navbar";
function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/boards" element={<BoardContent />}></Route>
          <Route path="/boards/:id" element={<ListContainer />}></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
