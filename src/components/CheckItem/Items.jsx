import { Box, Checkbox, Flex, IconButton, Text } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import UpdateFieldOnCheckList from "../Api/UpdateFieldOnCheckList";
import DeleteCheckItem from "../Api/DeleteCheckItem";
import { CloseIcon } from "@chakra-ui/icons";

const Items = ({
  checkItem,
  onDelCheckItem,
  onCheckboxChange,
  idCard,
  idCheckList,
}) => {
  const [isChecked, setIsChecked] = useState(false);
  const [isHovered, setIsHovered] = useState(false);

  useEffect(() => {
    setIsChecked(checkItem.state === "complete");
  }, []);

  const handleCheckboxToggle = async () => {
    setIsChecked(!isChecked);
    await UpdateFieldOnCheckList(
      idCard,
      checkItem.id,
      isChecked ? "incomplete" : "complete"
    );
    onCheckboxChange(!isChecked, checkItem.id);
  };

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleDeleteChekItem = async () => {
    await DeleteCheckItem(idCheckList, checkItem.id);
    onDelCheckItem(checkItem.id);
    // onCheckboxChange(false);
  };

  return (
    <Box>
      <Flex columnGap={2}>
        <Checkbox isChecked={isChecked} onChange={handleCheckboxToggle} />
        <Flex
          justifyContent="space-between"
          alignItems="center"
          w="full"
          h={9}
          py={1}
          px={2}
          borderRadius={15}
          _hover={{ bg: "gray.200" }}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <Text style={{ textDecoration: isChecked ? "line-through" : "none" }}>
            {checkItem.name}
          </Text>
          <IconButton
            icon={<CloseIcon />}
            size="xs"
            onClick={handleDeleteChekItem}
            visibility={isHovered ? "visible" : "hidden"}
          />
        </Flex>
      </Flex>
    </Box>
  );
};

export default Items;
