import React, { useState } from "react";
import { Button, Text, Input, Flex } from "@chakra-ui/react";
import CreateCheckItems from "../Api/CreateCheckItems";

export default function AddCheckItemButton({ checkListId, onAddCheckItem }) {
  const [isClicked, setIsClicked] = useState(false);
  const [checkItem, setCheckItem] = useState("");

  const handleClick = () => setIsClicked(true);

  const handleClose = () => {
    setIsClicked(false);
    setCheckItem("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    CreateCheckItems(checkListId, checkItem).then((createdCheckItem) => {
      onAddCheckItem(createdCheckItem);
      handleClose();
    });
  };

  return (
    <Flex alignItems="center">
      {isClicked ? (
        <form onSubmit={handleSubmit}>
          <Flex direction="column" ml={8}>
            <Input
              w="30rem"
              autoFocus
              pb={5}
              h={55}
              placeholder="Add an item"
              mb={2}
              fontSize="0.9rem"
              value={checkItem}
              onChange={(e) => setCheckItem(e.target.value)}
            />
            <Flex alignItems="center" columnGap={2}>
              <Button
                type="submit"
                bg="blue.600"
                color="white"
                size="sm"
                fontWeight="300"
              >
                Add
              </Button>
              <Button
                onClick={handleClose}
                size="xs"
                h="2rem"
                fontSize="0.9rem"
              >
                Cancel
              </Button>
            </Flex>
          </Flex>
        </form>
      ) : (
        <Button
          ml="1.4rem"
          mt="15px"
          mb="1.6rem"
          size="sm"
          bg="gray.300"
          justifyContent="flex-start"
          h="2rem"
          onClick={handleClick}
        >
          <Text fontSize="0.8rem" fontWeight="600">
            Add an item
          </Text>
        </Button>
      )}
    </Flex>
  );
}
