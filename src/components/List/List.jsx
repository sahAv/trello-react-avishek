import React from "react";
import { Box, Text, IconButton, Flex } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import ShowCards from "../Card/ShowCards";

const List = ({ list, onDelete }) => {
  const handleDeleteClick = () => {
    onDelete(list.id);
  };

  return (
    <Box bg="white" borderRadius="15px" w="15rem" p={2}>
      <Flex justifyContent="space-between" key={list.id}>
        <Text fontWeight="600" alignItems="center" pt="10px" pl="8px">
          {list.name}
        </Text>
        <IconButton
          bg="white"
          icon={<DeleteIcon />}
          onClick={handleDeleteClick}
        />
      </Flex>
      <ShowCards listId={list.id} listName={list.name}/>
    </Box>
  );
};

export default List;
