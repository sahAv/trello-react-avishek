import React, { useState, useEffect } from "react";
import { Flex, Button, useDisclosure, Text, Box } from "@chakra-ui/react";
import GetLists from "../Api/GetLists";
import CreateList from "../Api/CreateList";
import DeleteList from "../Api/DeleteList";
import InputModalBox from "../InputModalBox";
import { useParams } from "react-router-dom";
import SideBar from "../SideBar";
import List from "./List";

export default function ListContainer() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [lists, setLists] = useState([]);
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchLists = () => {
      setIsLoading(true);
      GetLists(id)
        .then((data) => {
          setLists(data);
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    };
    fetchLists();
  }, []);

  const handleAddList = async (listTitle) => {
    try {
      setIsLoading(true);
      setError(null);
      onClose();
      const createdList = await CreateList(id, listTitle);
      setLists((prevLists) => [...prevLists, createdList]);
      setIsLoading(false);
    } catch (error) {
      console.error("Error creating list:", error);
      setError(error);
      setIsLoading(false);
    }
  };

  const handleDeleteList = async (listId) => {
    try {
      await DeleteList(listId);
      setLists((prevLists) => prevLists.filter((list) => list.id !== listId));
    } catch (error) {
      console.error("Error deleting list:", error);
    }
  };
  return (
    <Box h="92.8vh" bg="blue.300" display="flex" px="12%" py="1.5rem">
      <SideBar />
      <Box>
        <Flex ml="4" overflowX="auto" columnGap="4">
          <Flex alignItems="flex-start" columnGap={4}>
            {lists.map((list) => (
              <List key={list.id} list={list} onDelete={handleDeleteList} />
            ))}
          </Flex>
          <Flex>
            <Button onClick={onOpen} w="15rem">
              + Add another list
            </Button>
          </Flex>
          <InputModalBox
            isOpen={isOpen}
            onClose={onClose}
            title={"List"}
            onAdd={handleAddList}
          />
        </Flex>
        <Flex justifyContent="center" w="100%">
          {isLoading && <Text fontSize="2rem">Loading...</Text>}
          {error && <Text color="red">Error: {error.message}</Text>}
        </Flex>
      </Box>
    </Box>
  );
}
