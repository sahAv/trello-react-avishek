import React from "react";
import { Box, Heading } from "@chakra-ui/react";
import CreateBoardBtn from "./CreateBoardBtn";
import SideBar from "../SideBar";

export default function BoardContent() {
  return (
    <Box display="flex" px="12%" py="1.5rem">
      <SideBar />
      <Box w="full" px="2rem" py="1rem">
        <Box>
          <Heading as="h2" size="md">
            YOUR WORKSPACES
          </Heading>
        </Box>
        <Box my={4}>Trello Avishek</Box>
        <Box display="flex" columnGap={5}>
          <CreateBoardBtn />
        </Box>
      </Box>
    </Box>
  );
}
