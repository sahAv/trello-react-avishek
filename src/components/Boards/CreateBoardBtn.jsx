import { Box, Button, Text, useDisclosure } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import CreateBoard from "../Api/CreateBoard";
import GetBoardDetails from "../Api/GetBoardDetails";
import InputModalBox from "../InputModalBox";

export default function CreateBoardBtn() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [boards, setBoards] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchBoards = () => {
      setIsLoading(true);
      GetBoardDetails()
        .then((data) => {
          setBoards(data);
          setIsLoading(false);
        })
        .catch((error) => {
          setIsLoading(false);
          setError("failed to create board");
        });
    };
    fetchBoards();
  }, []);

  const handleAddBoard = async (boardTitle) => {
    setIsLoading(true);
    setError(null);
    onClose();
    const createdBoard = await CreateBoard(boardTitle);
    setBoards((prevBoards) => [...prevBoards, createdBoard]);
    setIsLoading(false);
  };

  return (
    <Box display="flex" flexWrap="wrap" alignItems="center" gap={4}>
      {boards.map((board) => (
        <Link to={`${board.id}`} key={board.id}>
          <Button key={board.id} w={250} h={130} colorScheme="blue">
            {board.name}
          </Button>
        </Link>
      ))}
      <Button onClick={onOpen} w={250} h={130}>
        Create New Board
      </Button>
      {isLoading && <Text fontSize="1.5rem">Loading...</Text>}
      {error && <Text color="red">Error: {error}</Text>}
      <InputModalBox
        isOpen={isOpen}
        onClose={onClose}
        title={"Board"}
        onAdd={handleAddBoard}
      />
    </Box>
  );
}
