import React from "react";
import MenuBtn from "./MenuBtn";
import {
  Flex,
  Spacer,
  Text,
  Button,
  Box,
  InputGroup,
  InputLeftElement,
  Input,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
const Navbar = () => {
  return (
    <div>
      <Box
        display="flex"
        px={4}
        py={2}
        borderBottom={1}
        borderBottomColor={"black"}
      >
        <Flex w={720}>
          <Button w={51} bg={"transparent"}>
            <i className="fa-solid fa-ellipsis-vertical"></i>
            <Spacer />
            <i className="fa-solid fa-ellipsis-vertical"></i>
            <Spacer />
            <i className="fa-solid fa-ellipsis-vertical"></i>
          </Button>
          <Spacer />
          <Link to={"./boards"}>
            <Button bg={"transparent"}>
              <Text fontSize={25}>Trello</Text>
            </Button>
          </Link>
          <Spacer />
          <MenuBtn
            menuSelect="Workspaces"
            Icon={<i className={"fa-solid fa-chevron-down"}></i>}
          />
          <Spacer />
          <MenuBtn
            menuSelect="Recent"
            Icon={<i className={"fa-solid fa-chevron-down"}></i>}
          />
          <Spacer />
          <MenuBtn
            menuSelect="Starred"
            Icon={<i className={"fa-solid fa-chevron-down"}></i>}
          />
          <Spacer />
          <MenuBtn
            menuSelect="Templates"
            Icon={<i className={"fa-solid fa-chevron-down"}></i>}
          />
          <Spacer />
          <Button colorScheme="blue" size="md">
            {" "}
            Create
          </Button>
        </Flex>
        <Spacer />
        <Box display="flex">
          <InputGroup>
            <InputLeftElement pointerEvents="none">
              <i className={"fa-solid fa-magnifying-glass"} />
            </InputLeftElement>
            <Input type="text" placeholder={`Search`} />
          </InputGroup>
          <MenuBtn menuSelect={<i className="fa-regular fa-bell" />} />
          <MenuBtn
            menuSelect={<i className="fa-regular fa-circle-question" />}
          />
          <MenuBtn menuSelect={<i className="fa-solid fa-user" />} />
        </Box>
      </Box>
    </div>
  );
};

export default Navbar;
