import React, { useState } from "react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Input,
} from "@chakra-ui/react";

export default function InputModalBox({ isOpen, onClose, title, onAdd }) {
  const [inputTitle, setInputTitle] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (inputTitle.trim()) {
      onAdd(inputTitle);
      setInputTitle("");
    }
  };
  function handleClose() {
    onClose();
    setInputTitle("");
  }

  return (
    <Modal isOpen={isOpen} onClose={handleClose}>
      <ModalContent>
        <ModalHeader>{title} Title</ModalHeader>
        <form onSubmit={handleSubmit}>
          <ModalCloseButton />
          <ModalBody>
            <Input
              autoFocus
              placeholder={`Enter ${title} Title`}
              value={inputTitle}
              onChange={(e) => setInputTitle(e.target.value)}
            />
          </ModalBody>
          <ModalFooter>
            <Button type="submit" colorScheme="blue" mr={3}>
              Create
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
}
