import React from 'react'
import { Box, Button } from '@chakra-ui/react'
import { Link } from 'react-router-dom'

export default function SideBar({boards}) {
  return (
    <Box display="flex" flexDirection="column" w="13rem" rowGap="5px">
      <Link to="/boards">
      <Button w="full" display="flex" justifyContent="flex-start">Trello-Avi</Button>
      </Link>
      <Link to="/boards">
      <Button w="full" display="flex" justifyContent="flex-start">Boards</Button>
      </Link>
      <Button w="full" display="flex" justifyContent="flex-start">Members</Button>
      <Button w="full" display="flex" justifyContent="flex-start">Workspace</Button>
      <Button w="full" display="flex" justifyContent="flex-start">Table</Button>
    </Box>
  )
}
