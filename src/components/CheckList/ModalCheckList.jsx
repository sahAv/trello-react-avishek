import React, { useEffect, useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Text,
  Flex,
  Box,
  Button,
  Input,
} from "@chakra-ui/react";
import {
  CalendarIcon,
  HamburgerIcon,
  InfoIcon,
  ViewIcon,
} from "@chakra-ui/icons";
import CreateCheckList from "../Api/CreateCheckList";
import DeleteCheckList from "../Api/DeleteCheckList";
import GetCheckList from "../Api/GetCheckList";
import AddCheckList from "./AddCheckList";
import CheckList from "./CheckList";

export default function ModalCheckList({
  isOpen,
  onClose,
  cardName,
  listName,
  cardId,
}) {
  const [checkList, setCheckList] = useState([]);

  useEffect(() => {
    const fetchCheckList = async () => {
      try {
        const data = await GetCheckList(cardId);
        setCheckList(data);
      } catch (error) {
        console.error("Error fetching checklist: ", error);
      }
    };
    if (isOpen) {
      fetchCheckList();
    }
  }, [isOpen]);

  const handleCreateCheckList = async (newCheckList) => {
    try {
      const createdCheckList = await CreateCheckList(cardId, newCheckList);
      setCheckList((prevCheckList) => [...prevCheckList, createdCheckList]);
    } catch (error) {
      console.error("Error creating checklist:", error);
    }
  };

  const handleDeleteCheckList = async (checkListId) => {
    await DeleteCheckList(checkListId);
    setCheckList((prevCheckList) =>
      prevCheckList.filter((checkList) => checkList.id !== checkListId)
    );
  };

  const handleCheckListUpdate = (updatedCheckList) => {
    setCheckList((prevCheckList) =>
      prevCheckList.map((checkList) =>
        checkList.id === updatedCheckList.id ? updatedCheckList : checkList
      )
    );
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent
        bg="whitesmoke"
        maxW="45rem"
        py={5}
        px={1}
        borderRadius="1rem"
      >
        <ModalHeader
          display="flex"
          alignItems="center"
          columnGap={2}
          w="25rem"
          p={0}
          pl={5}
        >
          <CalendarIcon />
          <Text>{cardName}</Text>
        </ModalHeader>
        <Text ml={12} fontSize="14px">
          in list {listName}
        </Text>
        <Flex>
          <Box w="35rem">
            <ModalBody w="full">
              <Box m="1.3rem 2rem">
                <Text fontSize="0.8rem" fontWeight="600">
                  Notifications
                </Text>
                <Button size="sm" my={2} bg="gray.300">
                  <ViewIcon mr={2} /> Watch
                </Button>
              </Box>
              {checkList.map((checkLists) => (
                <CheckList
                  key={checkLists.id}
                  checkList={checkLists}
                  onDeleteCheckList={handleDeleteCheckList}
                  listName={cardId}
                  listId={checkLists.id}
                  checkItems={checkLists.checkItems}
                  onCheckListUpdate={handleCheckListUpdate}
                />
              ))}
            </ModalBody>
            <ModalFooter
              justifyContent="flex-start"
              flexDirection="column"
              rowGap={3}
            >
              <Flex justifyContent="space-between" w="full">
                <Flex alignItems="center" columnGap={3}>
                  <HamburgerIcon />
                  <Text fontWeight="600">Activity</Text>
                </Flex>
                <Button bg="gray.300" size="sm" fontSize="0.8rem">
                  Show Details
                </Button>
              </Flex>
              <Input
                bg="white"
                placeholder="Write a comment"
                ml="1.4rem"
                w="95%"
                borderRadius="0.8rem"
              />
            </ModalFooter>
          </Box>
          <Box display="flex" flexDirection="column" rowGap="7px">
            <Text fontSize="11px" fontWeight="600">
              Add to card
            </Text>
            <AddCheckList title={"Members"} />
            <AddCheckList title={"Labels"} />
            <AddCheckList
              title={"Checklist"}
              onCreateChecklist={handleCreateCheckList}
            />
            <AddCheckList title={"Dates"} />
            <AddCheckList title={"Attachment"} />
            <AddCheckList title={"Cover"} />
            <AddCheckList title={"Custom Fields"} />
            <Text fontSize="12px" fontWeight="600" mt={4}>
              Power-ups
            </Text>
            <AddCheckList title={"+ Add Power-Ups"} />
            <Flex alignItems="center" justifyContent="space-between" mt={4}>
              <Text fontSize="12px" fontWeight="600">
                Automation
              </Text>
              <InfoIcon />
            </Flex>
            <AddCheckList title={"+ Add Button"} />
            <Text fontSize="12px" fontWeight="600">
              Actions
            </Text>
            <AddCheckList title={"Move"} />
            <AddCheckList title={"Copy"} />
            <AddCheckList title={"Make Template"} />
            <AddCheckList title={"Archive"} />
            <AddCheckList title={"Share"} />
          </Box>
        </Flex>
      </ModalContent>
    </Modal>
  );
}
