import {
  Box,
  Button,
  Flex,
  FocusLock,
  Input,
  Popover,
  PopoverArrow,
  PopoverCloseButton,
  PopoverContent,
  PopoverTrigger,
  Spacer,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React, { useState } from "react";

const Form = ({ onCreateChecklist }) => {
  const [checkListName, setCheckListName] = useState("");

  const handleChange = (e) => {
    setCheckListName(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!checkListName.trim()) {
      return;
    }
    onCreateChecklist(checkListName);
    setCheckListName("");
  };

  return (
    <Stack as="form" spacing={4} onSubmit={handleSubmit}>
      <Box>
        <Text fontSize="0.8rem" fontWeight="600" m={1}>
          Title
        </Text>
        <Input
          autoFocus
          placeholder="Enter name"
          value={checkListName}
          onChange={handleChange}
        />
      </Box>
      <Button type="submit" colorScheme="blue" w="4rem" size="sm">
        Add
      </Button>
    </Stack>
  );
};

export default function AddCheckList({ title, onCreateChecklist }) {
  const { onOpen, onClose, isOpen } = useDisclosure();

  const handleCreatecheckList = (checkListName) => {
    onCreateChecklist(checkListName);
    onClose();
  };

  return (
    <Popover
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      placement="bottom"
      closeOnBlur={true}
    >
      <PopoverTrigger>
        <Button
          bg="gray.300"
          size="sm"
          w="8.5rem"
          display="flex"
          justifyContent="flex-start"
          fontSize="14px"
          fontWeight="600"
        >
          {title}
        </Button>
      </PopoverTrigger>
      <PopoverContent p={2}>
        <FocusLock returnFocus persistentFocus={false}>
          <PopoverArrow />
          <Flex h={7}>
            <Spacer />
            <Text fontSize="0.9rem" fontWeight="600">
              Add {title}
            </Text>
            <Spacer />
            <PopoverCloseButton />
          </Flex>
          <Form onCreateChecklist={handleCreatecheckList} />
        </FocusLock>
      </PopoverContent>
    </Popover>
  );
}
