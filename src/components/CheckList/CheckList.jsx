import React from "react";
import { Box, Button, Flex, Progress, Text } from "@chakra-ui/react";
import AddCheckItemButton from "../CheckItem/AddCheckItemButton";
import Items from "../CheckItem/Items";

const CheckList = ({ checkList, onDeleteCheckList, listName, listId, onCheckListUpdate }) => {
 
  const handleDeleteCheckList = () => {
    onDeleteCheckList(checkList.id);
  };
  
  const handleAddCheckItem = (createdCheckItem) => {
    const updatedCheckList = {
      ...checkList,
      checkItems: [...checkList.checkItems, createdCheckItem],
    };
    onCheckListUpdate(updatedCheckList);
  };

  const handleDelCheckItem = async (checkItemId) => {
    const updatedCheckList = {
      ...checkList,
      checkItems: checkList.checkItems.filter(
        (checkItem) => checkItem.id !== checkItemId
      ),
    };
    onCheckListUpdate(updatedCheckList);
  };

  const handleCheckboxChange = (isChecked, itemId) => {
    const updatedCheckList = {
      ...checkList,
      checkItems: checkList.checkItems.map((item) =>
        item.id === itemId
          ? { ...item, state: isChecked ? "complete" : "incomplete" }
          : item
      ),
    };
    onCheckListUpdate(updatedCheckList);
  };

  const progress =
    checkList.checkItems.length > 0
      ? (checkList.checkItems.filter((item) => item.state === "complete")
          .length /
          checkList.checkItems.length) *
        100
      : 0;

  return (
    <Box>
      <Flex
        justifyContent="space-between"
        key={checkList.id}
        cursor="pointer"
        borderRadius="10px"
      >
        <Flex alignItems="center">
          <Text p={2}>{checkList.name}</Text>
        </Flex>
        <Button bg="gray.300" fontSize="0.8rem" onClick={handleDeleteCheckList}>
          Delete
        </Button>
      </Flex>
      <Flex alignItems="center" ml="3px">
        <Text fontSize="12px" mr="5px">
          {Math.trunc(progress)}%
        </Text>
        <Progress
          flex="1"
          size="sm"
          bg="gray.300"
          borderRadius="5px"
          colorScheme={progress === 100 ? "green" : "blue"}
          value={progress}
        />
      </Flex>
      <Flex flexDirection="column" mt={2} ml={0.35}>
        {checkList.checkItems.map((checkitem) => (
          <Items
            key={checkitem.id}
            checkItem={checkitem}
            onDelCheckItem={handleDelCheckItem}
            onCheckboxChange={handleCheckboxChange}
            idCheckList={listId}
            idCard={listName}
          />
        ))}
      </Flex>
      <AddCheckItemButton
        checkListId={listId}
        onAddCheckItem={handleAddCheckItem}
      />
    </Box>
  );
};

export default CheckList;
