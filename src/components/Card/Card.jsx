import React, { useState } from "react";
import { Flex, Text, IconButton, useDisclosure } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import ModalChekList from "../CheckList/ModalCheckList";

export default function Card({ card, onDelete, listName }) {
  const [isHovered, setIsHovered] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleDeleteClick = (e) => {
    e.stopPropagation();
    onDelete(card.id);
  };

  return (
    <Flex
      justifyContent="space-between"
      key={card.id}
      bg="gray.100"
      cursor="pointer"
      borderRadius="10px"
      border="solid 2px white"
      transition="border-color 0.3s"
      _hover={{ borderColor: "blue" }}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={onOpen}
    >
      <Text p={2}>{card.name}</Text>
      <IconButton
        aria-label="Delete card"
        icon={<DeleteIcon />}
        onClick={handleDeleteClick}
        visibility={isHovered ? "visible" : "hidden"}
      />
      <ModalChekList
        isOpen={isOpen}
        onClose={onClose}
        cardName={card.name}
        listName={listName}
        cardId={card.id}
      />
    </Flex>
  );
}
