import React, { useState, useEffect } from "react";
import { Flex } from "@chakra-ui/react";
import GetCards from "../Api/GetCards";
import DeleteCard from "../Api/DeleteCard";
import AddCard from "./AddCard";
import Card from "./Card";

export default function ShowCards({ listId, listName }) {
  const [cards, setCards] = useState([]);

  useEffect(() => {
    const fetchCards = async () => {
      try {
        const data = await GetCards(listId);
        setCards(data);
      } catch (error) {
        console.error("Error fetching cards: ", error);
      }
    };
    fetchCards();
  }, []);

  const handleDeleteCard = async (cardId) => {
    try {
      await DeleteCard(cardId);
      setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
    } catch (error) {
      console.error("Error deleting card:", error);
    }
  };

  const handleAddCard = (newCard) => {
    setCards([...cards, newCard]);
  };

  return (
    <Flex direction="column" mt="2" rowGap={2}>
      {cards.map((card) => (
        <Card
          key={card.id}
          card={card}
          onDelete={handleDeleteCard}
          listName={listName}
        />
      ))}
      <AddCard listId={listId} onAddCard={handleAddCard} />
    </Flex>
  );
}
