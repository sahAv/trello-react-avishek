import React, { useState } from "react";
import { Button, Text, Input, Flex, IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import CreateCard from "../Api/CreateCard";

export default function AddCard({ listId, onAddCard }) {
  const [isClicked, setIsClicked] = useState(false);
  const [cardTitle, setCardTitle] = useState("");

  const handleClick = () => setIsClicked(true);
  const handleClose = () => {
    setIsClicked(false);
    setCardTitle("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!cardTitle.trim()) {
      handleClose();
      return;
    }
    try {
      const createdCard = await CreateCard(listId, cardTitle);
      onAddCard(createdCard);
      setCardTitle("");
      handleClose();
    } catch (error) {
      console.error("Error creating card:", error);
    }
  };

  return (
    <Flex alignItems="center">
      {isClicked ? (
        <Flex direction="column" w="100%">
          <form onSubmit={handleSubmit}>
            <Input
              autoFocus
              placeholder="Enter a title for this card..."
              mb={2}
              fontSize="0.9rem"
              value={cardTitle}
              onChange={(e) => setCardTitle(e.target.value)}
              onKeyDown={(e) => e.key === "Enter" && handleSubmit(e)}
            />
            <Flex alignItems="center">
              <Button
                type="submit"
                colorScheme="blue"
                mr={2}
                w="5rem"
                h="2rem"
                fontWeight="300"
              >
                Add card
              </Button>
              <IconButton
                icon={<CloseIcon />}
                onClick={handleClose}
                colorScheme="white"
                color="gray"
              />
            </Flex>
          </form>
        </Flex>
      ) : (
        <Button
          w="100%"
          justifyContent="flex-start"
          h="2rem"
          bg="white"
          onClick={handleClick}
        >
          <Text fontSize="1rem" fontWeight="300">
            + Add a card
          </Text>
        </Button>
      )}
    </Flex>
  );
}
