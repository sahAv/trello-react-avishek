import axios from "axios";
const key = "02538ae4c8c1f2a4d09209fb1df4b5b0";
const token =
  "ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E";

export default function CreateCheckItems(checkListId, checkItemName) {
  return axios
    .post(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${key}&token=${token}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.data;
      } else {
        throw new Error("Failed to create board");
      }
    })
    .catch((error) => {
      console.error("Error creating board:", error);
      throw error;
    });
}
