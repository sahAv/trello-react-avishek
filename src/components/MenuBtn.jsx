import { Menu, MenuButton, MenuItem, MenuList, Button } from "@chakra-ui/react";
import React from "react";

export default function MenuBtn({menuSelect, Icon}) {
  return (
    <div>
      <Menu>
        <MenuButton
          as={Button}
          bg={"transparent"}
          rightIcon={Icon}
        >
          {menuSelect}
        </MenuButton>
        <MenuList>
          <MenuItem>Trello</MenuItem>
        </MenuList>
      </Menu>
    </div>
  );
}
